import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PurchaseTableComponent } from './purchase-table/purchase-table.component';
import { PurchaseNewComponent } from './purchase-new/purchase-new.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [PurchaseTableComponent, PurchaseNewComponent]
})
export class PurchaseModule { }

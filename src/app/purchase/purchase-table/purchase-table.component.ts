import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Purchase } from '../purchase';

@Component({
  selector: 'app-purchase-table',
  templateUrl: './purchase-table.component.html',
  styleUrls: ['./purchase-table.component.css']
})
export class PurchaseTableComponent implements OnInit {

  private SERVER_PO = 'http://localhost:6003';

  public purchaseOrders = new Array<Purchase>();

  constructor(private http: Http) {}

  ngOnInit() {
      // http://31.220.55.236:6003/api/v1/purchase
      //this.purchaseOrders = [{id: "1a6s1c6s21v6", createdDate: "17/04/2017"}];
      this.http.get(this.SERVER_PO+'/api/v1/purchase').subscribe(resp => {
        this.purchaseOrders = resp.json(); 
      }, error => {
        console.log(error);
      }); 
  }

}

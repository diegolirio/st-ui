import { People } from './people';

export class Address {

	public id: String;
    public cep: String;
    public publicPlace: String;
    public neighborhood: String;
    public city: String;
	public number: number;
	//public State state;
	public people = new People();
	//public boolean active;    

}

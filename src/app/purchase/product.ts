export class Product {

    public id: String;
    public code: String;
    public description: String;
	public valueUnit: number;
	public productType: String;
}

import { People } from './people';

export class Telephone {

	public id: String;
    public contactType: String;
    public number: String;
    public people = new People();

}

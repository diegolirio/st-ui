import { Component, OnInit, Input } from '@angular/core';
import { Purchase } from '../purchase';
import { Http } from '@angular/http';
import { People } from '../people';
import { Address } from '../address';
import { Telephone } from '../telephone';
import { OrderProduct } from '../order-product';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../product';

@Component({
  selector: 'app-purchase-new',
  templateUrl: './purchase-new.component.html',
  styleUrls: ['./purchase-new.component.css']
})
export class PurchaseNewComponent implements OnInit {

  public formVisible: string = 'SENDER';
  public purchase = new Purchase();

  public addressSender = new Address();
  public phoneSender: Telephone;
  public customerSender = new People();
  public addressesSender = new Array<Address>();
  public phonesSender = new Array<Telephone>();

  public addressRecipient = new Address();
  public phoneRecipient: Telephone;
  public customerRecipient = new People();
  public addressesRecipient = new Array<Address>();
  public phonesRecipient = new Array<Telephone>();  

  public addressShippingCompany = new Address();
  public phoneShippingCompany: Telephone;
  public customerShippingCompany = new People();
  public addressesShippingCompany = new Array<Address>();
  public phonesShippingCompany = new Array<Telephone>();   

  public item = new OrderProduct();
  public itens = new Array<OrderProduct>();

  private SERVER = 'http://localhost:6002';//'http://31.220.55.236:5003/st-customer';
  private SERVER_PO = 'http://localhost:6003'; //'http://31.220.55.236:5003/st-purchase-order';//'http://31.220.55.236:5003/st-customer';
  private SERVER_PRODUCT = 'http://localhost:6001';

  constructor(private http: Http,  private actRoute: ActivatedRoute) { }

  ngOnInit() {
    let id = this.actRoute.snapshot.params['id'];
    console.log(id);
    if(id) {
        this.http.get(this.SERVER_PO+'/api/v1/purchase/'+id).subscribe(resp => {
            this.purchase = resp.json();
            this.findCustomerSenderByCpfCnpj(this.purchase.customerAddressSender.people.cpfCnpj);
            this.findCustomerRecipientByCpfCnpj(this.purchase.customerAddressRecipient.people.cpfCnpj);
            this.findCustomerShippingCompanyByCpfCnpj(this.purchase.customerAddressShippingCompany.people.cpfCnpj);
        });
    }
  }

  next() {
    if(this.formVisible == 'SENDER') {
      this.formVisible = 'RECIPIENT';
    }
    else if(this.formVisible == 'RECIPIENT') {
      this.formVisible = 'SHIPPING_COMPANY';
    }
    else if(this.formVisible == 'SHIPPING_COMPANY') {
      this.formVisible = 'PURCHASE';
    }   
    else if(this.formVisible == 'PURCHASE') {

      this.purchase.customerAddressSender = this.addressSender;
      this.purchase.customerAddressRecipient = this.addressRecipient;
      this.purchase.customerAddressShippingCompany = this.addressShippingCompany;
      this.purchase.phoneRecipient = this.phoneRecipient.number;
      this.purchase.phoneSender = this.phoneSender.number;
      this.purchase.phoneShippingCompany = this.phoneShippingCompany.number;
      
      let po = new Purchase();
      po.id = this.purchase.id;
      po.customerAddressRecipient = this.addressRecipient;
      //po.customerAddressRecipient.people = null;

      po.customerAddressSender = this.addressSender;
      po.customerAddressShippingCompany = this.addressShippingCompany;
      po.contactRecipient = this.purchase.contactRecipient = "First";
      
      po.phoneShippingCompany = this.purchase.phoneShippingCompany;
      po.phoneSender = this.purchase.phoneSender;
      po.phoneRecipient = this.purchase.phoneRecipient;
      
      po.contactRecipient = this.purchase.contactRecipient;
      po.paymentsTerms = this.purchase.paymentsTerms;
      po.typeFreight = this.purchase.typeFreight;
      po.createdDate = this.purchase.createdDate;
      po.representative = this.purchase.representative;
      po.remark = this.purchase.remark;
      po.status = this.purchase.status;

      console.log(JSON.stringify(po));
      this.save(po);
      //this.router.navigate(['/product-details', id]);
    }   
    else if(this.formVisible == 'ITENS') {
      console.log(this.itens); 
      if(!this.itens || this.itens.length == 0) {
          alert('Adicione um Item no minimo!');
          return;
      }
      this.formVisible = 'OVERVIEW';
    }   
    else if(this.formVisible == 'OVERVIEW') {
      this.completedPO(this.purchase);
      this.formVisible = 'MESSAGE';
      //console.log(JSON.stringify(this.purchase));
      //alert(JSON.stringify(this.purchase));
    }            
  }


  save(purchase) {
    this.http.post(this.SERVER_PO+'/api/v1/purchase', purchase).subscribe(resp => {
      this.purchase = resp.json();
      alert('Pedido gravado com sucesso');
      this.formVisible = 'ITENS';
    }, error => {
      console.log(error);
    });
  }


  findCustomerSenderByCpfCnpj(cpfCnpj) {
      //this.customerSender = {id:"1s5cx1sc", cpfCnpj: "35357359869", name: "Diego Lirio"};
      this.http.get(this.SERVER+'/api/v1/customer/cpfcnpj/'+cpfCnpj).subscribe(resp => {
        console.log(resp.json());
        //this.purchase.customerAddressSender.people = resp.json(); 
        this.customerSender = resp.json();
        this.findAddressByCustomerCpfCnpj(this.customerSender.cpfCnpj);
        this.findPhoneByCustomerCpfCnpj(this.customerSender.cpfCnpj);
      }, error => {
        console.log(error);
      });
  }

  findAddressByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/address/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.addressesSender = resp.json();
      if(this.purchase.id) {
          for(let a of this.addressesSender) {
            if(this.purchase.customerAddressSender.id == a.id) {
              this.addressSender = a;
            }
          }
      }
    }, error => {
      console.log(error);
    });
  }

  findPhoneByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/phone/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.phonesSender = resp.json();
      if(this.purchase.id) {
          for(let p of this.phonesSender) {
            if(this.purchase.phoneSender == p.number) {
              this.phoneSender = p;
            }
          }
      }      
    }, error => {
      console.log(error);
    });
  }

  findCustomerRecipientByCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/customer/cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.customerRecipient = resp.json();
      this.findAddressRecipientByCustomerCpfCnpj(this.customerRecipient.cpfCnpj);
      this.findPhoneRecipientByCustomerCpfCnpj(this.customerRecipient.cpfCnpj);
    }, error => {
      console.log(error);
    });
  }

  findAddressRecipientByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/address/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.addressesRecipient = resp.json();
      if(this.purchase.id) {
          for(let a of this.addressesRecipient) {
            if(this.purchase.customerAddressRecipient.id == a.id) {
              this.addressRecipient = a;
            }
          }
      }
    }, error => {
      console.log(error);
    });
  }

  findPhoneRecipientByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/phone/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.phonesRecipient = resp.json();
      if(this.purchase.id) {
          for(let p of this.phonesRecipient) {
            if(this.purchase.phoneRecipient == p.number) {
              this.phoneRecipient = p;
            }
          }
      }       
    
    }, error => {
      console.log(error);
    });
  }      



  findCustomerShippingCompanyByCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/customer/cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.customerShippingCompany = resp.json();
      this.findAddressShippingCompanyByCustomerCpfCnpj(this.customerShippingCompany.cpfCnpj);
      this.findPhoneShippingCompanyByCustomerCpfCnpj(this.customerShippingCompany.cpfCnpj);
    }, error => {
      console.log(error);
    });
  }

  findAddressShippingCompanyByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/address/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.addressesShippingCompany = resp.json();
      if(this.purchase.id) {
          for(let a of this.addressesShippingCompany) {
            if(this.purchase.customerAddressShippingCompany.id == a.id) {
              this.addressShippingCompany = a;
            }
          }
      }      
    }, error => {
      console.log(error);
    });
  }

  findPhoneShippingCompanyByCustomerCpfCnpj(cpfCnpj) {
    this.http.get(this.SERVER+'/api/v1/phone/people-cpfcnpj/'+cpfCnpj).subscribe(resp => {
      console.log(resp.json());
      this.phonesShippingCompany = resp.json();
      if(this.purchase.id) {
          for(let p of this.phonesShippingCompany) {
            if(this.purchase.phoneShippingCompany == p.number) {
              this.phoneShippingCompany = p;
            }
          }
      }      
    
    }, error => {
      console.log(error);
    });
  }    


  findProductByCode(productCode) {
      console.log(productCode);
      this.http.get(this.SERVER_PRODUCT+'/api/v1/product/code/'+productCode).subscribe(resp => {
          this.item.product = resp.json();
      }, error => {
          console.log(error); 
      });
  }
 
  saveItem(item: OrderProduct) {
    item.valueUnit = this.item.product.valueUnit;
    this.purchase.ordersProducts.push(item);
    this.save(this.purchase);
    //this.http.post(this.SERVER_PO+'/api/v1/item', item).subscribe(resp => {
    //  this.itens.push(resp.json());
    //}, error => {
    //  console.log(error);
    //});
  }

  deleteItem(item: OrderProduct) {
      //this.http.delete(this.SERVER_PO+'/api/v1/item/'+item.id).subscribe(resp => {
      //  this.findItens(this.purchase);
      //}, error => {
      //  console.log(error);
      //});
      let index = this.purchase.ordersProducts.indexOf(item);
      this.purchase.ordersProducts.splice(index,1);
      this.save(this.purchase);
  }

  completedPO(purchase: Purchase) {
      this.http.put(this.SERVER_PO+'/api/v1/purchase/complete',purchase).subscribe(resp => {
          this.purchase = resp.json();
      }, error => {
          console.log(error);
      });
  }

}

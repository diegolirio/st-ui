import { Purchase } from "./purchase";
import { Product } from "./product";

export class OrderProduct {

    public id: String;
    public purchaseOrder = new Purchase();
    public product = new Product();
    public valueUnit: number;
	public amount: number;

}

import { Address } from "./address";
import { OrderProduct } from "./order-product";

export class Purchase {

    public id: String;
    public createdDate: String;
    public customerAddressSender = new Address();
    public phoneSender: String;
    public customerAddressRecipient = new Address();
    public phoneRecipient: String;
    public contactRecipient: String;
    public customerAddressShippingCompany = new Address();
    public phoneShippingCompany: String;
    public paymentsTerms: String;
    public remark: String;
    public representative: String;
    public typeFreight: String;
    public status: String;
    public ordersProducts = new Array<OrderProduct>();
    
}

import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseTableComponent } from './purchase/purchase-table/purchase-table.component';
import { PurchaseNewComponent } from './purchase/purchase-new/purchase-new.component';

const APP_ROUTES: Routes = [
    { path: '',            component: PurchaseTableComponent },
    { path: 'novo-pedido', component: PurchaseNewComponent },
    { path: 'novo-pedido/:id', component: PurchaseNewComponent }
    //{ path: '**', component: DashComponent } 
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);